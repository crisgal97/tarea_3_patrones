﻿using System;
using System.IO;
using PatronesCreacionales;
using AtributosCalidad;

namespace AtributosCalidad
{
    class Seguridad
    {
        private string tipo;

        public Seguridad(string tipo)
        {
            this.tipo = tipo;
        }
    }

    class ToleranciaFallos
    {
        public string tipo;

        public ToleranciaFallos(string tipo)
        {
            this.tipo = tipo;
        }
    }

    class Portabilidad
    {
        private string tipo;

        public Portabilidad(string tipo)
        {
            this.tipo = tipo;
        }
    }

    class Interoperabilidad
    {
        private string tipo;

        public Interoperabilidad(string tipo)
        {
            this.tipo = tipo;
        }
    }

    class Rendimiento
    {
        private string tipo;

        public Rendimiento(string tipo)
        {
            this.tipo = tipo;
        }
    }

    class Escalabilidad
    {
        private string tipo;

        public Escalabilidad(string tipo)
        {
            this.tipo = tipo;
        }
    }

    class Mantenibilidad
    {
        private string tipo;

        public Mantenibilidad(string tipo)
        {
            this.tipo = tipo;
        }
    }
}

namespace PatronesCreacionales
{
    abstract class Creator
    {
        public abstract Product FactoryMethod();
    }

    abstract class Product
    {
    }

    class Singleton : Product
    {
        private static Singleton? instance;

        private Singleton()
        {
        }

        public static Singleton GetInstance()
        {
            if (instance == null)
            {
                instance = new Singleton();
            }
            return instance;
        }
    }

    class FactoryMethodCreator : Creator
    {
        public override Product FactoryMethod()
        {
            return new ProductA();
        }
    }

    class AbstractFactory : Creator
    {
        public override Product FactoryMethod()
        {
            return new ProductB();
        }

        public Product CreateProductA()
        {
            return new ProductA();
        }

        public Product CreateProductB()
        {
            return new ProductB();
        }
    }

    class ProductA : Product
    {
    }

    class ProductB : Product
    {
    }
}

namespace Dominio
{
    abstract class Vehicle
    {
        public string? type;

        protected Vehicle()
        {
            type = ""; // Initialize the type field with an empty string
        }

        public abstract void Accelerate();

        public abstract void MoveForward();

        public abstract void Brake();

        public abstract void Reverse();
    }

    class ElectricVehicle : Vehicle
    {
        private int batteryCapacity;

        public ElectricVehicle(int batteryCapacity)
        {
            type = "Electric Vehicle";
            this.batteryCapacity = batteryCapacity;
        }

        public override void Accelerate()
        {
            // Logic to accelerate an electric vehicle
        }

        public override void MoveForward()
        {
            // Logic to move forward an electric vehicle
        }

        public override void Brake()
        {
            // Logic to brake an electric vehicle
        }

        public override void Reverse()
        {
            // Logic to reverse an electric vehicle
        }
    }

    class CombustionVehicle : Vehicle
    {
        private int fuelCapacity;

        public CombustionVehicle(int fuelCapacity)
        {
            type = "Combustion Vehicle";
            this.fuelCapacity = fuelCapacity;
        }

        public override void Accelerate()
        {
            // Logic to accelerate a combustion vehicle
        }

        public override void MoveForward()
        {
            // Logic to move forward a combustion vehicle
        }

        public override void Brake()
        {
            // Logic to brake a combustion vehicle
        }

        public override void Reverse()
        {
            // Logic to reverse a combustion vehicle
        }
    }
}


namespace ClasesDominio
{
    class Vehicle
    {
        public string type = ""; // Initialize the type field with an empty string

        public void Accelerate()
        {
            Console.WriteLine($"The {type} vehicle is accelerating.");
        }

        public void MoveForward()
        {
            Console.WriteLine($"The {type} vehicle is moving forward.");
        }

        public void Brake()
        {
            Console.WriteLine($"The {type} vehicle is braking.");
        }

        public void Reverse()
        {
            Console.WriteLine($"The {type} vehicle is reversing.");
        }
    }

    class Factory
    {
        public Vehicle CreateElectricVehicle(int speed)
        {
            Vehicle electricVehicle = new Vehicle();
            electricVehicle.type = "Electric";
            // Additional code related to creating an electric vehicle goes here
            return electricVehicle;
        }

        public Vehicle CreateCombustionVehicle(int speed)
        {
            Vehicle combustionVehicle = new Vehicle();
            combustionVehicle.type = "Combustion";
            // Additional code related to creating a combustion vehicle goes here
            return combustionVehicle;
        }
    }

    class PruebaFactory
    {
        private static int testsPerformed = 0;

        public void RunVehicleTests(Vehicle vehicle)
        {
            // Perform tests on the given vehicle
            // ...
            Console.WriteLine($"Running tests on {vehicle.type} vehicle...");
        }

        public void GenerateTestReport(Vehicle vehicle)
        {
            // Increment the testsPerformed count
            testsPerformed++;

            // Generate a test report for the given vehicle
            // ...
            Console.WriteLine($"Generating test report for {vehicle.type} vehicle...");

            // Check if the count reaches 5
            if (testsPerformed == 5)
            {
                // Generate the file name
                string fileName = $"pruebas_{GetFormattedDateTime()}.log";

                // Write the test report to the file
                using (StreamWriter fileWriter = new StreamWriter(fileName))
                {
                    fileWriter.WriteLine($"Fecha y Hora: {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}");
                    fileWriter.WriteLine("Pruebas realizadas:");
                    fileWriter.WriteLine($"- Electric Vehicle: 5 pruebas");
                    fileWriter.WriteLine($"- Combustion Vehicle: 5 pruebas");
                }

                // Reset the testsPerformed count
                testsPerformed = 0;
            }
        }

        private string GetFormattedDateTime()
        {
            return DateTime.Now.ToString("yyyyMMdd_HH_mm");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            FactoryMethodCreator factoryMethodCreator = new FactoryMethodCreator();
            AbstractFactory abstractFactory = new AbstractFactory();
            Factory vehicleFactory = new Factory();
            string[] sucursales = { "Sucursal A", "Sucursal B", "Sucursal C" };
            string[] turnos = { "Turno 1", "Turno 2", "Turno 3" };
            // Crear el sistema de pruebas
            PruebaFactory pruebaFactory = new PruebaFactory();

            foreach (string sucursal in sucursales)
            {
                Console.WriteLine($"Sucursal: {sucursal}");

                foreach (string turno in turnos)
                {
                    Console.WriteLine($"Turno: {turno}");

                    // Crear vehículos y realizar pruebas
                    Vehicle electricVehicle = vehicleFactory.CreateElectricVehicle(100);
                    Vehicle combustionVehicle = vehicleFactory.CreateCombustionVehicle(50);

                    // Obtener productos de las fábricas
                    Product productA = factoryMethodCreator.FactoryMethod();
                    Product productB = abstractFactory.FactoryMethod();
                    Product productAFromAbstractFactory = abstractFactory.CreateProductA();
                    Product productBFromAbstractFactory = abstractFactory.CreateProductB();

                    // Realizar pruebas a los vehículos
                    for (int k = 0; k < 5; k++)
                    {
                        electricVehicle.Accelerate();
                        electricVehicle.MoveForward();
                        electricVehicle.Brake();
                        electricVehicle.Reverse();

                        combustionVehicle.Accelerate();
                        combustionVehicle.MoveForward();
                        combustionVehicle.Brake();
                        combustionVehicle.Reverse();
                    }

                    // Realizar pruebas y generar informe
                    pruebaFactory.RunVehicleTests(electricVehicle);
                    pruebaFactory.GenerateTestReport(electricVehicle);

                    pruebaFactory.RunVehicleTests(combustionVehicle);
                    pruebaFactory.GenerateTestReport(combustionVehicle);

                    // Mostrar los productos desarrollados para cada turno
                    Console.WriteLine("Productos desarrollados para el turno:");
                    Console.WriteLine($"- ProductA (Factory Method): {productA.GetType().Name}");
                    Console.WriteLine($"- ProductB (Abstract Factory): {productB.GetType().Name}");
                    Console.WriteLine($"- ProductA (Abstract Factory): {productAFromAbstractFactory.GetType().Name}");
                    Console.WriteLine($"- ProductB (Abstract Factory): {productBFromAbstractFactory.GetType().Name}");
                    Console.WriteLine();
                }
            
            }

             // Create an instance of ToleranciaFallos
            ToleranciaFallos toleranciaFallos = new ToleranciaFallos("Resilient");

            // Simulate a system operation
            Console.WriteLine("Performing a critical system operation...");

            // Check the type of fault tolerance
            if (toleranciaFallos.tipo == "Resilient")
            {
                // Perform the operation with resilient fault tolerance
                PerformOperationWithResilience();
            }
            else if (toleranciaFallos.tipo == "Redundant")
            {
                // Perform the operation with redundant fault tolerance
                PerformOperationWithRedundancy();
            }
            else
            {
                // Unknown fault tolerance type
                Console.WriteLine("Unknown fault tolerance type. Unable to perform the operation.");
            }
                Console.WriteLine("Pruebas finalizadas");
        }

        static void PerformOperationWithResilience()
        {
            // Logic to perform the operation with resilient fault tolerance
            Console.WriteLine("Performing the operation with resilient fault tolerance...");
            Console.WriteLine("Operation completed successfully!");
        }

        static void PerformOperationWithRedundancy()
        {
            // Logic to perform the operation with redundant fault tolerance
            Console.WriteLine("Performing the operation with redundant fault tolerance...");
            Console.WriteLine("Operation completed successfully!");
        }
            
    }
}
